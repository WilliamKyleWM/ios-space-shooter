//
//  GameScene.swift
//  RetroSpace
//
//  Created by William Kyle on 1/14/21.
//

import SpriteKit
import GameplayKit

//globalized
var gameScore = 0
class GameScene: SKScene, SKPhysicsContactDelegate {
    

    let scoreLabel = SKLabelNode()
    let gameLabel = SKLabelNode()
    var holder = 1.0
    var bridge = 5
    
    let player = SKSpriteNode(imageNamed: "ship1")
    let bulletSound = SKAction.playSoundFileNamed("pew.mp3", waitForCompletion: false)
    let explosionSound = SKAction.playSoundFileNamed("explode.mp3", waitForCompletion: false)
    let music = SKAction.playSoundFileNamed("song.mp3", waitForCompletion: false)
    var moving = false
    
    
    enum gameState{
        case preGame
        case inGame
        case afterGame
    }
    
    var currentGameState = gameState.inGame;
    
    
    struct PhysicsCategories{
        static let None: UInt32 = 0 //0 (duh)
        static let Player: UInt32 = 0b1 //1
        static let Bullet: UInt32 = 0b10 //2
        static let Asteroid: UInt32 = 0b100 //4
    }
    
    
    func random() -> CGFloat{
        return CGFloat(Float(arc4random())/0xFFFFFFFF)
    }
    func random(min min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
        
    }
    
    
    let gameArea: CGRect
    override init(size: CGSize){
        
        
        let maxAspectRatio = CGFloat(16.0/9.0)
        let playableWidth = size.height/maxAspectRatio
        let margin = (size.width - playableWidth) / 2
        gameArea = CGRect(x: margin, y:0, width: playableWidth, height: size.height)
        
        
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func startTimer(){
        let timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(moveBackground), userInfo: nil, repeats: true)

        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
        timer.fire()
        }

    @objc func gravity(){
        if (!moving){
            player.position.y-=10
            
        }
        if (moving){
            player.position.y+=10
        }
        if player.position.y < gameArea.minY+575{
            player.position.y = gameArea.minY+575
        }
    }
    
    func startLevel(){
        
        if (self.action(forKey: "spawningAsteroids") != nil){
            self.removeAction(forKey: "spawningAsteroids")
        }
        holder += 1
        var levelDuration = Float(1.0)
        
        
        levelDuration = Float(holder/(holder*holder))
        
        let spawn = SKAction.run(spawnAsteroid)
        let waitToSpawn = SKAction.wait(forDuration: TimeInterval(levelDuration))
        let spawnSequence = SKAction.sequence([waitToSpawn,spawn])
        let spawnForever = SKAction.repeatForever(spawnSequence)
        self.run(spawnForever, withKey: "spawningAsteroids")
    
    }
    
    
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        gameScore = 0
        
        startTimer()
        let timer = Timer.scheduledTimer(timeInterval: 0.01, target: self,
                selector: #selector(gravity), userInfo: nil, repeats: true)
        startLevel()
        /*
        let spawntimer = Timer.scheduledTimer(timeInterval: 1.2, target: self,
                selector: #selector(spawner), userInfo: nil, repeats: true)*/
        
    
        player.setScale(6)
        player.position = (CGPoint(x: self.size.width/6,y: self.size.height/2))
        player.zPosition = 2
        player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        player.physicsBody!.affectedByGravity = false
        player.physicsBody!.categoryBitMask = PhysicsCategories.Player
        player.physicsBody!.collisionBitMask = PhysicsCategories.None
        player.physicsBody!.contactTestBitMask = PhysicsCategories.Asteroid
        
        self.addChild(player)
        
        scoreLabel.text = "Score: 0"
        scoreLabel.fontSize = 70
        scoreLabel.fontColor = SKColor.white
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        scoreLabel.position = CGPoint(x: self.size.height*0.15,y: self.size.width*0.9)
        scoreLabel.zPosition = 100
        self.addChild(scoreLabel)
        
        let playMusic = SKAction.sequence([music])
        //player.run(playMusic)
        
    }
    
    
    func addScore(){
        gameScore += 10
        scoreLabel.text = "Score: \(gameScore)"
        
        if gameScore%100 == 0{
            startLevel()
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var body1 = SKPhysicsBody()
        var body2 = SKPhysicsBody()
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            body1 = contact.bodyA
            body2 = contact.bodyB
        }
        
        else{
            body1 = contact.bodyB
            body2 = contact.bodyA
        }
        
        if body1.categoryBitMask == PhysicsCategories.Player && body2.categoryBitMask == PhysicsCategories.Asteroid{
            //player hit asteroid
            if (body1.node != nil){
            spawnExplosion(spawnPosition: body1.node!.position, height: body1.node!.xScale)
            }
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
            
            gameOver()
        }
        
        if body1.categoryBitMask == PhysicsCategories.Bullet && body2.categoryBitMask == PhysicsCategories.Asteroid{
            //bullet hit asteroid
            
            if (body2.node != nil){
            spawnExplosion(spawnPosition: body2.node!.position, height: body2.node!.xScale)
            }
            addScore()
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
        }
    }
    
    
    func spawnExplosion(spawnPosition: CGPoint, height: CGFloat){
        let explosion = SKSpriteNode(imageNamed: "boom")
        explosion.position = spawnPosition
        explosion.setScale(0)
        explosion.zPosition = 3
        self.addChild(explosion)
        
        let scaleIn = SKAction.scale(to: height, duration: 0.1)
        let fadeOut = SKAction.fadeOut(withDuration: 0.1)
        let delete = SKAction.removeFromParent()
        let explosionSequence = SKAction.sequence([explosionSound,scaleIn,fadeOut,delete])
        explosion.run(explosionSequence)
    }
    
    @objc func moveBackground(){
        let background = SKSpriteNode(imageNamed: "space")
        background.name="Background"
        background.size = self.size
        background.position = CGPoint(x: self.size.width/2,y: self.size.height/2)
        background.zPosition = 0
        self.addChild(background)
        
        let background2 = SKSpriteNode(imageNamed: "space")
        background2.name="Background2"
        background2.size = self.size
        background2.position = CGPoint(x: self.size.width+self.size.width/2,y: self.size.height/2)
        background2.zPosition = 0
        self.addChild(background2)
        
        let moveBackground = SKAction.moveTo(x:-background.size.width/2, duration: 4)
        let deleteBackground = SKAction.removeFromParent()
        let backgroundSequence = SKAction.sequence([moveBackground,deleteBackground])
        //Background 2
        let moveBackground2 = SKAction.moveTo(x:self.size.width/2, duration: 4)
        let deleteBackground2 = SKAction.removeFromParent()
        let backgroundSequence2 = SKAction.sequence([moveBackground2,deleteBackground2])
        
        background.run(backgroundSequence)
        background2.run(backgroundSequence2)
        //background.run(backgroundSequence2)
        
    }
    
    func fireBullet(){
        let bullet = SKSpriteNode(imageNamed: "bullet")
        bullet.name="Bullet"
        bullet.setScale(5)
        bullet.position = player.position
        bullet.zPosition = 1
        bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
        bullet.physicsBody!.affectedByGravity = false
        bullet.physicsBody!.categoryBitMask = PhysicsCategories.Bullet
        bullet.physicsBody!.collisionBitMask = PhysicsCategories.None
        bullet.physicsBody!.contactTestBitMask = PhysicsCategories.Asteroid
        
        self.addChild(bullet)
        
        let moveBullet = SKAction.moveTo(x: self.size.height+bullet.size.height, duration: 1)
        let deleteBullet = SKAction.removeFromParent()
        let bulletSequence = SKAction.sequence([bulletSound,moveBullet,deleteBullet])
        
        bullet.run(bulletSequence)
    }
    
    func spawnAsteroid(){
        let randomY = random(min:gameArea.minY+500,max:gameArea.maxY-500)
        
        let startPoint = CGPoint(x: self.size.width*1.2,y: randomY)
        let endPoint = CGPoint(x: -self.size.width*1.2,y: randomY)
        
        let speed_size = random(min: 3,max: 13)
        
        let asteroid = SKSpriteNode(imageNamed: "cookie1")
        asteroid.name="Asteroid"
        asteroid.setScale(speed_size)
        asteroid.position = startPoint
        asteroid.zPosition = 2
        asteroid.physicsBody = SKPhysicsBody(rectangleOf: asteroid.size)
        asteroid.physicsBody!.affectedByGravity = false
        asteroid.physicsBody!.categoryBitMask = PhysicsCategories.Asteroid
        asteroid.physicsBody!.collisionBitMask = PhysicsCategories.None
        asteroid.physicsBody!.contactTestBitMask = PhysicsCategories.Player | PhysicsCategories.Bullet
        if (currentGameState == gameState.inGame){
        self.addChild(asteroid)
        }
        let moveAsteroid = SKAction.move(to: endPoint, duration: TimeInterval(speed_size+random(min: 1, max: 5))/4)
        let deleteAsteroid = SKAction.removeFromParent()
        let asteroidSequence = SKAction.sequence([moveAsteroid,deleteAsteroid])
        asteroid.run(asteroidSequence)
    }
    

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
            let location = touch.location(in: self.view)
        if (currentGameState == gameState.inGame){

        if location.x>self.size.height/9{
            fireBullet()
        }
        if location.x<self.size.height/9{
            moving = true
            player.position.y += 10
        }
        }
        
    }
    
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
            let location = touch.location(in: self.view)
        
        let move = touches.first?.previousLocation(in: self.view)
        if (currentGameState == gameState.inGame){
        if location.x<=self.size.height/9{
            moving = true
            player.position.y += 10
        }
        
        else if move!.x>=self.size.height/9{
            moving = false
        }
        if player.position.y > gameArea.maxY-550{
            player.position.y = gameArea.maxY-550
        }
        }
    }

    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
            let location = touch.location(in: self.view)
        let move = touches.first?.previousLocation(in: self.view)
        if location.x<=self.size.height/9{
            moving = false
        }
        else if move!.x>=self.size.height/9{
            moving = false
        }
    }
    
    
    func gameOver(){
        currentGameState = gameState.afterGame
        
        gameLabel.text = "GAME OVER"
        gameLabel.fontSize = 100
        gameLabel.fontColor = SKColor.white
        gameLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        gameLabel.position = CGPoint(x: self.size.width/2-500,y: self.size.height/2)
        gameLabel.zPosition = 100
        self.addChild(gameLabel)
        
        let changeSceneAction = SKAction.run(changeScene)
        let wait = SKAction.wait(forDuration: 1)
        let sequence = SKAction.sequence([wait,changeSceneAction])
        self.run(sequence)
        
    }
    
    func changeScene(){
        let sceneToMoveTo = GameOverScene(size: self.size)
        sceneToMoveTo.scaleMode = self.scaleMode
        let transition = SKTransition.fade(withDuration: 1)
        self.view!.presentScene(sceneToMoveTo, transition: transition)
    }
    
    
    
    
}
