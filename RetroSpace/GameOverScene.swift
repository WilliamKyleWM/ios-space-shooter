//
//  GameOver.swift
//  RetroSpace
//
//  Created by William Kyle on 1/14/21.
//

import Foundation
import SpriteKit

class GameOverScene:SKScene{
    let restart = SKLabelNode()
    
    override func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "space")
        background.name="Background"
        background.size = self.size
        background.position = CGPoint(x: self.size.width/2,y: self.size.height/2)
        background.zPosition = 0
        self.addChild(background)
        
        let restart = SKLabelNode()
        restart.text = "RESTART"
        restart.fontSize = 100
        restart.fontColor = SKColor.white
        restart.color = SKColor.red
        restart.zPosition = 1
        restart.position = CGPoint(x: self.size.width/2,y: self.size.height/2+350)
        self.addChild(restart)
        
        let gameOverLabel = SKLabelNode()
        gameOverLabel.text = "YOUR SCORE"
        gameOverLabel.fontSize = 200
        gameOverLabel.fontColor = SKColor.red
        gameOverLabel.zPosition = 1
        gameOverLabel.position = CGPoint(x: self.size.width/2,y: self.size.height/2)
        self.addChild(gameOverLabel)
        
        let score = SKLabelNode()
        score.text = "\(gameScore)"
        score.fontSize = 100
        score.fontColor = SKColor.red
        score.zPosition = 1
        score.position = CGPoint(x: self.size.width/2,y: self.size.height/2 - 200)
        self.addChild(score)
        
        
        let defaults = UserDefaults()
        var highScoreNumber = defaults.integer(forKey: "saved")
        
        if gameScore > highScoreNumber{
            highScoreNumber = gameScore
            defaults.set(highScoreNumber, forKey: "saved")
        }
        
        let highscore = SKLabelNode()
        highscore.text = "High Score = \(highScoreNumber)"
        highscore.fontSize = 100
        highscore.fontColor = SKColor.white
        highscore.zPosition = 1
        highscore.position = CGPoint(x: self.size.width/2,y: self.size.height/2 - 400)
        self.addChild(highscore)

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
            let location = touch.location(in: self.view)
        
        if location.y<=self.size.width/25{
                let sceneToMoveTo = GameScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                let transition = SKTransition.fade(withDuration: 1)
                self.view!.presentScene(sceneToMoveTo, transition: transition)
            
        }
    }
}
    
